import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Human_modules_one", # Replace with your own username
    version="0.0.1",
    author="Kacper Wenta",
    author_email="kacperakawenta@gmail.com",
    description="Module for HR applications",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/YashtaZ/pakiety",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)