#!/usr/bin/env python3
# public _protected __private

__author__ = "Kacper Wenta"
__copyright__ = "Copyright (c) 2020 Kacper Wenta"
__email__ = "kacperakawenta@gmail.com"
__version__ = "1.0"

class Human:
    def __init__(self, name, surname):
        """
        Human class
        :param name:
        :param surname:
        """
        self._name = name
        self._surname = surname

    @property
    def name(self):
        return self._name

    @property
    def surname(self):
        return self._surname
