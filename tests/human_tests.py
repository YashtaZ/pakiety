#!/usr/bin/env python3

__author__ = "Kacper Wenta"
__copyright__ = "Copyright (c) 2020 Kacper Wenta"
__email__ = "kacperakawenta@gmail.com"
__version__ = "1.0"

from modules_one.human import Human
import unittest

class HumanTest(unittest.TestCase):
    def test_jan_kowalski(self):
        jan = Human("Jan", "Kowalski")
        self.assertEqual("Jan", jan.name, "Names don't match")
        self.assertEqual("Kowalski", jan.surname, "Surnames don't match")

